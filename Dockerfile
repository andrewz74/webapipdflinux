#https://xmlgraphics.apache.org/fop/0.95/embedding.html
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /app

# copy all files
COPY ./ ./
RUN dotnet restore

# Publish
WORKDIR /app
RUN dotnet publish -c Release -o out

FROM debian:10 as runtine
WORKDIR /app
LABEL maintainer="az"
RUN apt update
#RUN apt install fop -y
ENV PATH "$PATH:/usr/local/fop-2.5/fop"
RUN apt-get install -y default-jre
RUN apt-get install -y wget
RUN wget http://ftp.download-by.net/apache/xmlgraphics/fop/binaries/fop-2.5-bin.tar.gz
RUN tar -xvzf fop-2.5-bin.tar.gz -C /usr/local

# dotnet
RUN wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN apt-get update -y
RUN apt-get install -y apt-transport-https
RUN apt-get install -y aspnetcore-runtime-5.0

ENV ASPNETCORE_Environment=Production
ENV ASPNETCORE_URLS http://+:5000
EXPOSE 5000/tcp
COPY --from=build /app/out ./
COPY --from=build /app/fonts.fo ./

ENTRYPOINT ["/bin/bash"]
ENTRYPOINT ["dotnet","ApiPdf.dll"]
