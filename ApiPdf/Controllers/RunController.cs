﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;
using ApiPdf.Services;
using System.Xml.Linq;

namespace ApiPdf.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class RunController : ControllerBase
	{
		private readonly ILogger<RunController> _logger;
		private readonly IHelper _helper;

		public RunController(ILogger<RunController> logger, IHelper helper)
		{
			_logger = logger;
			_helper = helper;
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] XElement content, [FromQuery] string outputFileName)
		{
			string foDocument = content.ToString();
			string foFilename = _helper.GetRandomFileName(foDocument);
			string pdfFilename = _helper.GetRandomFileName();

			if (string.IsNullOrEmpty(outputFileName))
			{
				outputFileName = "output.pdf";
			}

			_logger.LogInformation($"outputFilename = {outputFileName}");
			_logger.LogInformation($"foFilename = {foFilename}");
			_logger.LogInformation($"pdfFilename = {pdfFilename}");
			_logger.LogInformation($"Xml content\r\n---{foDocument}\r\n---");

			try
			{
				ProcessStartInfo startInfo = new ProcessStartInfo()
				{
					FileName = "fop",
					Arguments = $"{foFilename} {pdfFilename}",
					RedirectStandardOutput = true,
					RedirectStandardError = true,
					UseShellExecute = false,
					CreateNoWindow = true,
				};

				using Process proc = new Process()
				{
					StartInfo = startInfo,
				};

				proc.Start(); // Start...
				await proc.WaitForExitAsync(); // ... and wait

				string standardOutput = await proc.StandardOutput.ReadToEndAsync();
				_logger.LogDebug($"output standard: §{standardOutput}§");
				string errorOutput = await proc.StandardError.ReadToEndAsync();
				_logger.LogDebug($"error: §{errorOutput}§");

				if (proc.ExitCode != 0)
				{
					_logger.LogError($"Error 500: {errorOutput}");
					return StatusCode(500, errorOutput);
				}
				else
				{
					byte[] file = System.IO.File.ReadAllBytes(pdfFilename);
					_logger.LogInformation($"Pdf size: {file?.Length}");
					return File(file, "application/pdf", outputFileName);
				}
					
			}
			catch (System.Exception ex)
			{
				_logger.LogError($"Error 500: Exception '{ex.Message}'");
				return StatusCode(500, ex.Message);
			}
			finally
			{
				if (_helper.DeleteFileCache)
				{
					if (!string.IsNullOrEmpty(foFilename))
					{
						System.IO.File.Delete(foFilename);
					}

					if (!string.IsNullOrEmpty(pdfFilename))
					{
						System.IO.File.Delete(pdfFilename);
					}
				}
			}
		}

		/*[HttpGet]
		public async Task<IActionResult> Get()
		{
			// https://stackoverflow.com/questions/61029223/how-to-execute-a-shell-command-across-platforms-in-net-core
			// https://stackoverflow.com/questions/23679283/c-sharp-execute-a-terminal-command-in-linux
			// https://loune.net/2017/06/running-shell-bash-commands-in-net-core/

			string dirToUse="/app/";
			//dirToUse="/home/az/Documents/msstash/webapipdflinux/";

			ProcessStartInfo startInfo = new ProcessStartInfo() {
				//FileName = "/bin/bash",
				//Arguments = "/dev/init.d/mnw stop",
				FileName="fop",
				Arguments=$"{dirToUse}fonts.fo {dirToUse}fonts.pdf",
				//FileName = "ping",
				//Arguments = "-c3 8.8.8.8",
				RedirectStandardOutput = true,
				RedirectStandardError = true,
				UseShellExecute = false,
				CreateNoWindow = true,
			};

			Process proc = new Process() {
				StartInfo = startInfo,
			};
			proc.Start();
			//string output1 = await proc.StandardOutput.ReadToEndAsync();
			//Console.WriteLine(output1);
			//string output11 = await proc.StandardError.ReadToEndAsync();
			//Console.WriteLine(output11);
			await proc.WaitForExitAsync();
			string output2 = await proc.StandardOutput.ReadToEndAsync();
			Console.WriteLine($"output standard: §{output2}§");
			string output21 = await proc.StandardError.ReadToEndAsync();
			Console.WriteLine($"error: §{output21}§");
			if (proc.ExitCode!=0)
			{
				return StatusCode(500, output21);
			}

			//return Ok("ciao");
			byte[] file = System.IO.File.ReadAllBytes($"{dirToUse}fonts.pdf");

	        return File(file, "application/pdf", "fonts.pdf");
		}*/
	}
}
