﻿using iText.Html2pdf;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ApiPdf.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class HtmlToPdfController : ControllerBase
	{
		private readonly ILogger<HtmlToPdfController> _logger;

		public HtmlToPdfController(ILogger<HtmlToPdfController> logger)
		{
			_logger = logger;
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromQuery] string outputFileName)
		{
			try
			{
				if (string.IsNullOrEmpty(outputFileName))
				{
					outputFileName = "output.pdf";
				}

				string htmlContent = null;
				using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
				{
					htmlContent = await reader.ReadToEndAsync();
				}

				_logger.LogInformation($"outputFilename = {outputFileName}");
				_logger.LogInformation($"html content\r\n---{htmlContent}\r\n---");

				var ms = new MemoryStream();
				HtmlConverter.ConvertToPdf(htmlContent, ms);
				return File(ms.ToArray(), "application/pdf", outputFileName);
			}
			catch (Exception ex)
			{
				_logger.LogError($"Error 500: Exception '{ex.Message}'");
				return StatusCode(500, ex.Message);
			}
		}
	}
}
