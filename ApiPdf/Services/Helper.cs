﻿using System;
using System.IO;
using System.Linq;

namespace ApiPdf.Services
{
	public interface IHelper
	{
		string GetRandomFileName();
		string GetRandomFileName(string content);
		bool DeleteFileCache { get; }
	}

	public class Helper : IHelper
	{
		private readonly string _directoryCache;
		private readonly bool _deleteCacheFile;
		public Helper()
		{
			if (bool.TryParse(Environment.GetEnvironmentVariable("DELETE_CACHE_FILE"), out bool result))
			{
				_deleteCacheFile = result;
			}
			else
			{
				_deleteCacheFile = true;
			}

			string directoryCache = Environment.GetEnvironmentVariable("DIRECTORY_CACHE") ?? "Cache";
			var di = new DirectoryInfo(directoryCache);
			if (di.Exists == false)
			{
				di.Create();
			}

			_directoryCache = di.FullName;

			// Check if exist files and delete it
			di.GetFiles().ToList().ForEach(t => File.Delete(t.FullName));
		}

		public string GetRandomFileName()
		{
			string filename;
			do
			{
				filename = Path.Combine(_directoryCache, Guid.NewGuid().ToString());
			} while (File.Exists(filename));

			return filename;
		}

		public string GetRandomFileName(string content)
		{
			string fileName = GetRandomFileName();
			if (!string.IsNullOrEmpty(content))
			{
				File.WriteAllText(fileName, content);
			}

			return fileName;
		}

		public bool DeleteFileCache
		{
			get
			{
				return _deleteCacheFile;
			}
		}
	}
}
